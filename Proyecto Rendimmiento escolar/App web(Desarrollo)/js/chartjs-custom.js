$(document).ready(function() {


    var doughnutData = [
        {
            value: 30,
            color:"#F7464A"
        },
        {
            value : 50,
            color : "#46BFBD"
        },
        {
            value : 100,
            color : "#FDB45C"
        },
        {
            value : 40,
            color : "#949FB1"
        },
        {
            value : 120,
            color : "#4D5360"
        }

    ];
    var lineChartData = {
        labels : ["0","5","10","15","20","25","30"],
        datasets : [
            {
                fillColor : "rgba(220,220,220,0.5)",
                strokeColor : "rgba(220,220,220,1)",
                pointColor : "rgba(220,220,220,1)",
                pointStrokeColor : "#fff",
                data : [15,13,7,3,2,1,0]
            },
            {
                fillColor : "rgba(151,187,205,0.5)",
                strokeColor : "rgba(151,187,205,1)",
                pointColor : "rgba(151,187,205,1)",
                pointStrokeColor : "#fff",
                data : [19,11,9,6,5,4,2]
            }
        ]

    };
    var pieData = [
        {
            value: 0.50390625,
            color:"#F38630"
        },
        {
            value : 0.49609375,
            color : "#E0E4CC"
        }

    ];
    var barChartData = {
        labels : [0,5,10,15,20,25],
        datasets : [
            {
                fillColor : "rgba(220,220,220,0.5)",
                strokeColor : "rgba(220,220,220,1)",
                data : [0.19921875,
                       0.19140625,
                       0.1640625,
                       0.11328125,
                       0.08203125,
                       0.07421875,
                       0.04296875,
                       0.03515625,
                       0.0234375,
                       0.01953125,
                       0.015625,
                       0.0078125,
                       0.00390625,
                       0.00390625,
                       0.00390625,
                       0.00390625,
                       0.00390625,
                       0.00390625,
                       0.00390625,
                       0.00390625]
            }
        ]

    };
    var chartData = [
        {
            value : .42578125,
            color: "#D97041"
        },
        {
            value : .41015625,
            color: "#C7604C"
        },
        {
            value : .1328125,
            color: "#21323D"
        },
        {
            value : .03125,
            color: "#9D9B7F"
        },

    ];
    var radarChartData = {
        labels : ["","","","","","",""],
        datasets : [
            {
                fillColor : "rgba(220,220,220,0.5)",
                strokeColor : "rgba(220,220,220,1)",
                pointColor : "rgba(220,220,220,1)",
                pointStrokeColor : "#fff",
                data : [65,59,90,81,56,55,40]
            },
            {
                fillColor : "rgba(151,187,205,0.5)",
                strokeColor : "rgba(151,187,205,1)",
                pointColor : "rgba(151,187,205,1)",
                pointStrokeColor : "#fff",
                data : [28,48,40,19,96,27,100]
            }
        ]

    };
    new Chart(document.getElementById("doughnut").getContext("2d")).Doughnut(doughnutData);
    new Chart(document.getElementById("line").getContext("2d")).Line(lineChartData);
    new Chart(document.getElementById("radar").getContext("2d")).Radar(radarChartData);
    new Chart(document.getElementById("polarArea").getContext("2d")).PolarArea(chartData);
    new Chart(document.getElementById("bar").getContext("2d")).Bar(barChartData);
    new Chart(document.getElementById("pie").getContext("2d")).Pie(pieData);


});
