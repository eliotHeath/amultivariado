import pickle
import numpy as np
from sklearn.cluster import KMeans as kmeans
from flask import Flask
from bottle import Bottle, request, abort, response
import pandas as pd
import os
import datetime
import warnings
warnings.filterwarnings("ignore")

apiAR = Bottle()
@apiAR.route('/metodo', method=['POST'])
def rend():


    lr = pickle.load(open('Rendimiento_escolar','rb'))

    print( '\n'
             + '+-----------------------------------------------------------+\n'
             + '   Kenner: Bienvenido, ahora se te hara una pequena prueba \n'
             + '          con base en tu historia para dar un diagnostico   \n'
             + '          de tu Rendimiento academico. Disfrutalo            \n'
             + '+-----------------------------------------------------------+\n')

    print('¿Cual es tu edad?')
    edad = int(input())
    print('¿Cual es tu Promedio Actual?')
    prom = float(input())
    print('¿Cuántas materias llevas este semestre?')
    mater = int(input())
    print('¿Cuántas horas estas en la escuela?')
    esc = int(input())
    print('¿Cuántas materias has reprobado a lo largo de la carrera?')
    rep = int(input())
    print('¿Cuántos exámenes extraordinarios has presentado?')
    ex = int(input())
    print('¿Cuánto tiempo tardas en llegar a la escuela? (min)')
    tar = int(input())
    print('¿Cuánto tiempo estudias al dia? (min)')
    est = int(input())

    x = [[0,0,edad,prom,mater,esc,rep,ex,tar,est,0]]

    print(kmeans.predict(x,[[0,0,0,0,0,0,0,0,0,]]))


    if lr.predict(x)[0]==.5:
    	print('Tu rendimiento puede mejorar, tienes que estudiar mas')
    else:
    	print('Vas bien, pero no dejes de estudiar')


if __name__ == '__main__':
    #app.run( host='0.0.0.0', port=3000, reloader=True)
    apiAR.run( )
