#===============================================
#===============Eliot Heathcliff================
#===============================================


import pandas as pd
import re

#==============================================
#===========Strings to ceros===================

def num(col):
    for i in range(len(col)):
        if re.match('([\d]{1,}[\s][A-Za-z]{1,})|([\d]{1,}[A-Za-z]{1,})|([A-Za-z]{1,})',col[i]):
            col[i] = 0

    return 'Sin strings'        

#==============================================
#===========NaN to ceros===================

def nan(col):
    
    NAN = col.isna()
    for i in range(len(NAN)):
        if NAN[i] == True:
            col[i] = 0
            
    return 'Sin vacios' 

#==============================================
#===========NaN to ceros===================

def s2f(col):

	for i in range(len(col)):

		col[i] = float(col[i]) 

	return 'Float'	